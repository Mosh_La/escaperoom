# EscapeRoom

Psychology project for Dawson College

### Team members 
| Name  | Role |
| ------------- | ------------- |
| Lara Mezirovsky  | Developer  |
| Nicholas Apanian  | Developer  |

### Tools
* React.js
* Node.js (MySQL)
* SASS

### Pre-reqs

* Run the sql script provided on slack 

* Make sure mysql server is up & running

* In case you get an error when running one of the commands bellow, just do __npm install__ and re-run the command again! (it might take a while...)

* Must have node installed (will install automatically npm)

### Getting started (package.json config)

* To start both server and client instances using the VS terminal
```bash
npm run dev
```

* To start client instance only 
```bash
npm run start
```

* To start server instance only
```bash
npm run server
```
