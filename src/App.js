import React, { Component } from 'react';
import './styles/style.scss';
import logo  from './logo.svg';

class App extends Component {
  state = {
    tests: [],
    test: {
      id: '', //hard coded for now... how to make it auto?
      text: ''
   }
  }

  componentDidMount() {
    this.getTests();
  }


  getTests = _ => {
    fetch('http://localhost:4000/test')
    .then(response => response.json())
    .then(response => this.setState({tests: response.data}))
    .catch(err =>console.error(err));
  }

  addTest = _ => {
    const {test} = this.state;
    console.log(test.id);
    console.log(test.text);
  fetch(`http://localhost:4000/test/add?id=${test.id}&text=${test.text}`)
  .then(this.getTests)
  .catch(err =>console.err(err));
  }

  renderTests =  ({test_id, test_message}) =>
  <div key={test_id}>{test_message}</div>


  render() {
    const {tests, test} = this.state;

    return (
      <div className="app">
        <div className="appContainer">
          <img className="app-svg" src={logo} alt="Logo" />
          {tests.map(this.renderTests)}
          Text:<input value={test.text}  
              onChange={e =>this.setState({test: {...test, text: e.target.value}})}></input>
          <button onClick={this.addTest}>Add</button>
        </div>
      </div>

    );
  }
}

export default App;
