/**
 * This is used to "Export" the mysql connection
 * We would have to import within every file that we use the mysql crud methods
 */
const mysql = require('mysql');

const connection = mysql.createConnection({
     host: 'localhost',
     user: 'root',
     password: 'dawson',
     database: 'escaperoom'
 });

 connection.connect(err =>  {
  if(err){
      return err;
  }
});

 module.exports = {
    connection
  }