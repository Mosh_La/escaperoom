/**
 * This file is the server file
 * It will serve as creating "end points"
 */
const {SELECT_ALL_FROM_TEST, INSERT_TEST} = require ('./queryList');
const {connection} = require ('./connection'); // test with: console.log(connection);
 const express = require('express'); //middleware for api
 const cors = require('cors');

 const app = express();
 app.use(cors());

 /**
  * NOTE!!!!!
 * To manually start the server from the terminal:
 * 1) nodemon index.js //I have installed nodemon which you can see the version of it in package.json, this is a pacakge that simplly allows you to start the node server 
 * 2) npm run serve // this is a goal that I have defined under "scripts" in the package.json file
 */
 app.listen (4000, () => {
    console.log("This server is listening on port 4000");
 });

 /**
  * ****************************************** Some end points ******************************************
  */

  //try http://localhost:4000/
 app.get('/', (req, res) => {
  res.send('Hello, this is a test');
    /**
     * Personal node:
     * get() takes a path and the response and the request
     * to print something to the server we use the response object
     * request is to get info from the query string that was given by another page
     */
 });


 //try http://localhost:4000/test
 app.get('/test', (req, res) => {
    connection.query(SELECT_ALL_FROM_TEST, (err, results ) => {
        if(err){
            return res.send(err);
        }
        else{
            return res.json({
                data: results
            });
        }
    });
 });


 
 //try http://localhost:4000/test/add
 app.get('/test/add', (req, res) => {
    const object = req.query; 
    const INSERT_TEST = `INSERT INTO TEST VALUES (0, '${object.text}')`; //fix duplicate primary key
    connection.query(INSERT_TEST, (err, results) => {
        if(err){
            return res.send(err);
        }
        else {
            return res.send("Adding a new test");
        }
    });
 });